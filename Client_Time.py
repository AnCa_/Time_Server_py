
import socket
import select
import time

print  "		Time Client designed by							"
print  "     ___           ___           ___           ___     	"
print  "    /\  \         /\__\         /\  \         /\  \    	"
print  "   /::\  \       /::|  |       /::\  \       /::\  \   	"
print  "  /:/\:\  \     /:|:|  |      /:/\:\  \     /:/\:\  \  	"
print  " /::\~\:\  \   /:/|:|  |__   /:/  \:\  \   /::\~\:\  \ 	"
print  "/:/\:\ \:\__\ /:/ |:| /\__\ /:/__/ \:\__\ /:/\:\ \:\__\	"
print  "\/__\:\/:/  / \/__|:|/:/  / \:\  \  \/__/ \/__\:\/:/  /	"
print  "     \::/  /      |:/:/  /   \:\  \            \::/  / 	"
print  "     /:/  /       |::/  /     \:\  \           /:/  /  	"
print  "    /:/  /        /:/  /       \:\__\         /:/  / 	"
print  "    \/__/         \/__/         \/__/         \/__/     "
print  " \n 					  \n  						\n  "

ip_config = []
UDP_PORT=15000


s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM,socket.IPPROTO_UDP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)

s1 = socket.socket(socket.AF_INET,socket.SOCK_DGRAM,socket.IPPROTO_UDP)

s2 = socket.socket(socket.AF_INET,socket.SOCK_STREAM)


def Broadcast() : 
	message = "conf"
	s.sendto(message,("255.255.255.255",UDP_PORT))
	print "sending config..."
	(nData, address) = s.recvfrom(100)

	intnData = int(nData)

	while intnData > 0 : 

			(string, address) = s.recvfrom(100) 
			ip_config.append(string)
			intnData = intnData - 1


	host1=ip_config[0]
	port1=ip_config[1]
	print "-" * 60
	print "one host is up at " 
	print ip_config
	print "-" * 60
	time.sleep (2)

	s.close
	return ip_config

def Unicast(ip_config) : 


	host1=ip_config[0]
	port1=ip_config[1]
	message = "fonction"
	s1.sendto(message,(host1,int(port1)))
	(nData, address) = s1.recvfrom(100)

	intnData = int(nData) 
	while intnData > 0 : 

			(string, address) = s1.recvfrom(100) 
			cmd = string
			intnData = intnData - 1
	print "-" * 60
	print "requested cmd is " + cmd
	print "-" * 60
	time.sleep (2)	
	s1.close
	return cmd

def Clock(ip_config, cmd) : 

	host1=ip_config[0]
	port1=ip_config[1]
	s2.connect((host1,int(port1)))
	print "-" * 60
	print "connection OK"
	print "-" * 60

	while 1 :
		s2.sendall(cmd)
		message=s2.recv(1024)
		print "-" * 60
		print "current Time is : " + message
		print "-" * 60
		time.sleep(1)

def main():

	ip_config = Broadcast()
	cmd = Unicast(ip_config)
	Clock(ip_config, cmd)

main();
