
import socket
from threading import Thread
import sys
from datetime import datetime
import time 


print  "		Time Server designed by							" 
print  "     ___           ___           ___           ___     	"
print  "    /\  \         /\__\         /\  \         /\  \    	"
print  "   /::\  \       /::|  |       /::\  \       /::\  \   	"
print  "  /:/\:\  \     /:|:|  |      /:/\:\  \     /:/\:\  \  	"
print  " /::\~\:\  \   /:/|:|  |__   /:/  \:\  \   /::\~\:\  \ 	"
print  "/:/\:\ \:\__\ /:/ |:| /\__\ /:/__/ \:\__\ /:/\:\ \:\__\	"
print  "\/__\:\/:/  / \/__|:|/:/  / \:\  \  \/__/ \/__\:\/:/  /	"
print  "     \::/  /      |:/:/  /   \:\  \            \::/  / 	"
print  "     /:/  /       |::/  /     \:\  \           /:/  /  	"
print  "    /:/  /        /:/  /       \:\__\         /:/  / 	"
print  "    \/__/         \/__/         \/__/         \/__/     "
print  " \n 					  \n  						\n  "

# detection de l'addresse IP du Server
UDP_IP = [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][0]
UDP_PORT = 15000
UDP_PORT_2 = 1337

adresse = (UDP_IP,UDP_PORT)
print(adresse)


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)
sock2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
socket3 = socket.socket(socket.AF_INET,socket.SOCK_STREAM) # TCP

def print_f(message) : 

	print "-" * 60
	print message
	print "-" * 60

def Broadcast(): # Thread de broadcast UDP 

	try: 
		sock.bind(("",UDP_PORT))

	except ValueError as message:
		print (message)
		sys.exit(1)

	while 1 : 


		data, addr = sock.recvfrom(1024)
		print ("Server connected by", addr)
		print "requested : " + data

		if data == "conf" : 

			nData = 2
			sock.sendto(str(nData),addr)
			sock.sendto(str(UDP_IP),addr)
			sock.sendto(str(UDP_PORT_2),addr)

		else:
			nData = 1
			sock.sendto(str(nData),addr)
			sock.sendto("error in cmd",addr)


def  Sensor(): 

	try: 
		sock2.bind((UDP_IP,UDP_PORT_2))

	except ValueError as message:
		print (message)
		sys.exit(1)
		
	while 1:

		data, addr = sock2.recvfrom(1024)
		print ("Server connected by ", addr)

		if data == "fonction" : 
			nData = 1
			sock.sendto(str(nData),addr)
			sock2.sendto("Time?",addr)

		else:
			nData = 1
			sock.sendto(str(nData),addr)
			sock2.sendto("error in cmd",addr)

def handleClient(connection, address):
	while 1:
		data = connection.recv(1024)
		Time = datetime.now()
		if data == "Time?":
			connection.send(str(Time))

		else:
			connection.send("error in cmd")
		print address
		print data
	connection.close()
#process en attente de connexion
#demarre un thread independant a chaque connexion etablie def dispatcher():

def dispatcher():
	print_f("Thread TCP Sensor start")

	try:
		socket3.bind((UDP_IP,UDP_PORT_2))
		socket3.listen(5)

	except ValueError as message:
		print (message)
		sys.exit(1)

	while 1:
		connection, address = socket3.accept()
		print ("Server connected by", address)
		tcp_client=Thread(target=handleClient, args=(connection,address,))
		tcp_client.start()



def main():


	udp_client_B = Thread(target=Broadcast, args=())
	udp_client_B.start()
	print_f("Thread UDP Broadcast start")

	udp_client_S = Thread(target=Sensor, args=())
	udp_client_S.start()
	print_f("Thread UDP Sensor start")

	dispatcher()



main();
